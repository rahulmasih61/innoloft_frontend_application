import React, {useState} from "react";
import dropDownButton from '../CSS Modules/dropdown.module.css'

export default function DropDownButton({selected, options, label, updateHandler}) {
    let [containerState, setContainerState] = useState(false)
    return <div>
        <div className={dropDownButton.heading}>{label}</div>
        <div className={dropDownButton.label} onClick={() => {
            setContainerState(!containerState)
        }
        }>{selected}
            <div className={"fa fa-caret-down"}/>
        </div>
        {
            containerState && options.map(item => <div key={item} onClick={() => {
                updateHandler(item)
                setContainerState(false);
            }} className={dropDownButton.dropdownItem}>{item}
            </div>)
        }
    </div>
}