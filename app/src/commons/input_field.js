import React, {useState} from "react";
import inputField from '../CSS Modules/inputField.module.css'

export default function InputField({label, value, onchangeHandler, error}) {
    return <div style={{width: "auto"}}>
        <div className={inputField.label}>{label}</div>
        <input  value={value}
               onChange={(event => onchangeHandler(event.currentTarget.value))} type="text"
               className={inputField.inputField}/>
        {error && <div className={inputField.alert}>{error}</div>}
    </div>
}