import React from "react";
import button from '../CSS Modules/button.module.css'

export default function Button({onClickButton}) {
    return <button onClick={(e => onClickButton())} className={button.primary}>Update</button>
}