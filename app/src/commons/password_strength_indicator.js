import React from "react";
import passwordStrength from '../CSS Modules/password_strength.module.css';

export default function PasswordStrengthIndicator({currentLength}) {
    return <div className={passwordStrength.container}>
        <div className={currentLength <= 0 ? passwordStrength.bar : passwordStrength.bar1}/>
        <div className={currentLength <= 2 ? passwordStrength.bar : passwordStrength.bar2}/>
        <div className={currentLength <= 4 ? passwordStrength.bar : passwordStrength.bar3}/>
        <div className={currentLength <= 6 ? passwordStrength.bar : passwordStrength.bar4}/>
        <div className={currentLength <= 8 ? passwordStrength.bar : passwordStrength.bar5}/>
    </div>
}