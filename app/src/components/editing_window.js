import React, {useState} from "react";
import BasicDataTab from "./basic_data_tab";
import PersonalDataTab from "./personal_data_tab";
import editingWindow from '../CSS Modules/editing_window.module.css'

export default function EditingWindow() {
    let [currentTab, setCurrentTab] = useState(0);
    return <div className={editingWindow.main}>
        <div className={editingWindow.toggleWindow}>
            <div className={currentTab === 0 ? editingWindow.toggleItem : editingWindow.toggleItemUnselected}
                 onClick={() => {
                     setCurrentTab(0)
                 }}>Basic Data
            </div>
            <div className={currentTab === 1 ? editingWindow.toggleItem : editingWindow.toggleItemUnselected}
                 onClick={() => {
                     setCurrentTab(1)
                 }}>Address
            </div>
        </div>
        {currentTab === 0 && <BasicDataTab/>}
        {currentTab === 1 && <PersonalDataTab/>}
    </div>
}