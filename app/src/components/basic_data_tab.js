import React, {useContext, useState} from "react";
import InputField from "../commons/input_field";
import Button from "../commons/button";
import Joi from 'joi-browser';
import PasswordStrengthIndicator from "../commons/password_strength_indicator";
import {UserContext} from '../contexts/user'
import {fakeAsyncCall} from "../utils/asyncronous_calls";
import {pageValidator, fieldValidator} from "../utils/validator";

import inputField from "../CSS Modules/inputField.module.css";

export default function BasicDataTab() {
    const {userData} = useContext(UserContext);
    const {basicData} = userData;
    const {values, setters} = basicData;
    let [pwd, setPwd] = useState('')
    let [confirmPwd, setConfirmPwd] = useState('')
    let [key, setKey] = useState(1)
    let [errors, setErrors] = useState({
        email: null,
        pwd: null,
        confirmPwd: null
    });
    let [isMakingRequest, updateRequestState] = useState(false)
    let [showRequestStatus, updateRequestStatusVisibility] = useState(false)

    const schema = {
        email: Joi.string().email().required().label('Email'),
        pwd: Joi.string().min(4).required().label('Password'),
        confirmPwd: Joi.equal(pwd.toString()).required().label('Confirm Password')
    }

    function validate(value, fieldName) {
        fieldValidator(value, fieldName, errors, schema, setErrors)
    }

    async function showStatus() {
        await fakeAsyncCall(() => {
            updateRequestStatusVisibility(true)
        }, 3000)
        updateRequestStatusVisibility(false)
    }


    return <div key={key}>
        <InputField label={"Email"} value={values.email} onchangeHandler={(value) => {
            setters.setEmail(value);
            validate(value, "email")
        }} error={errors.email}/>
        <InputField label={"Password"} value={pwd} onchangeHandler={(value) => {
            setPwd(value);
            validate(value, "pwd")
        }} error={errors.pwd}/>
        <PasswordStrengthIndicator currentLength={pwd.length}/>
        <InputField label={"Confirm Password"} value={confirmPwd} onchangeHandler={(value) => {
            setConfirmPwd(value);
            validate(value, "confirmPwd")
        }} error={errors.confirmPwd && "\"Confirm Password\" must be equal to \"Password\""}/>
        {!isMakingRequest && <Button onClickButton={async () => {
            const isValidated = pageValidator({...values, pwd: pwd, confirmPwd: confirmPwd}, schema, setErrors, errors);
            setKey(++key)
            if (isValidated) {
                await fakeAsyncCall(() => {
                    updateRequestState(true)
                }, 5000)
                updateRequestState(false);
                showStatus();
            }
        }}/>}
        {
            isMakingRequest && <div className={inputField.label}>Making Request...</div>
        }
        {
            showRequestStatus && <div className={inputField.label}>Submitted Successfully</div>
        }
    </div>
}