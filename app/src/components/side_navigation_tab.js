import React from "react";
import sideNavigation from '../CSS Modules/side_navigation.module.css'

export default function SideNavigationTab() {
    return <div className={sideNavigation.tabBody}>
        <div className={sideNavigation.tabItem}>
            <div className={`fa fa-home ${sideNavigation.faIcon}`}/>
            Home
        </div>
        <div className={sideNavigation.tabItem}>
            <div className={`fa fa-bullhorn ${sideNavigation.faIcon}`} />
            My Account</div>
        <div className={sideNavigation.tabItem}>
            <div className={`fa fa-building ${sideNavigation.faIcon}` } />
            My Company</div>
        <div className={sideNavigation.tabItem}>
            <div className={`fa fa-cog ${sideNavigation.faIcon}`} />
            My Settings</div>
        <div className={sideNavigation.tabItem}>
            <div className={`fa fa-newspaper-o ${sideNavigation.faIcon}`} />
            News</div>
        <div className={sideNavigation.tabItem}>
            <div className={`fa fa-area-chart ${sideNavigation.faIcon}`} />
            Analytics</div>
    </div>
}