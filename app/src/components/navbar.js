import React, {useState} from "react";
import SideNavigationTab from "./side_navigation_tab";
import logo from '../assets/logo_innoloft.png';
import navbar from '../CSS Modules/navbar.module.css';

export default function NavBar(props) {
    let [isToggled, setToggle] = useState(false)
    return <div className={navbar.main}>
        <img className={navbar.logo} src={logo} alt=""/>
        <ToggleButton isOpened={isToggled} toggleHandler={() => setToggle(!isToggled)}/>
        {isToggled &&
        <div className={navbar.toggleContainer}>
            <SideNavigationTab/>
        </div>}
    </div>
}

function ToggleButton({isOpened, toggleHandler}) {
    return (
        <div className={`${navbar.xcontainer} ${isOpened && navbar.xchange}`} onClick={(e) => toggleHandler()}>
            <div className={navbar.xbar1}/>
            <div className={navbar.xbar2}/>
            <div className={navbar.xbar3}/>
        </div>
    )
}
