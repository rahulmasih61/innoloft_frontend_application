import React, {useContext, useState} from "react";
import InputField from "../commons/input_field";
import Button from "../commons/button";
import DropDownButton from "../commons/dropdown_button";
import {UserContext} from "../contexts/user";
import {fakeAsyncCall} from "../utils/asyncronous_calls";
import {fieldValidator, pageValidator} from "../utils/validator";
import {PersonalDataSchema} from "../utils/joi_validation_schema";
import inputField from "../CSS Modules/inputField.module.css";

export default function PersonalDataTab() {
    const {userData} = useContext(UserContext);
    const {addressData} = userData;
    const {values, setters} = addressData;
    let [key, setKey] = useState(1)
    let [errors, setErrors] = useState({
        firstName: null,
        lastName: null,
        street: null,
        houseNum: null,
        postal: null,
    });
    let [showRequestStatus, updateRequestStatusVisibility] = useState(false)
    let [isMakingRequest, updateRequestState] = useState(false)

    const schema = PersonalDataSchema;

    function validateField(value, fieldName) {
        fieldValidator(value, fieldName, errors, schema, setErrors)
    }

    async function showStatus() {
        await fakeAsyncCall(() => {
            updateRequestStatusVisibility(true)
        }, 3000)
        updateRequestStatusVisibility(false)
    }

    return <div key={key}>
        <InputField label={"First Name"} value={values.firstName} onchangeHandler={(value) => {
            setters.setFirstName(value);
            validateField(value, "firstName");
        }} error={errors["firstName"]}/>
        <InputField label={"Last Name"} value={values.lastName} onchangeHandler={(value) => {
            setters.setLastName(value);
            validateField(value, "lastName");
        }} error={errors["lastName"]}/>
        <div className={inputField.label}>Address :</div>
        <InputField label={"Street"} value={values.street} onchangeHandler={(value) => {
            setters.setStreet(value);
            validateField(value, "street");
        }} error={errors["street"]}/>
        <InputField label={"House Number"} value={values.houseNum} onchangeHandler={(value) => {
            setters.setHouseNum(value);
            validateField(value, "houseNum");
        }} error={errors["houseNum"]}/>
        <InputField label={"Postal Code"} value={values.postal} onchangeHandler={(value) => {
            setters.setPostal(value);
            validateField(value, "postal");
        }} error={errors["postal"]}/>
        <DropDownButton label={'Country'} updateHandler={(val) => {
            setters.setCountry(val)
        }} selected={values.country} options={['Germany', 'Austria', 'Switzerland']}/>
        {!isMakingRequest && <Button onClickButton={async () => {
            let currentState = {...values};
            delete currentState.country;
            const isValidated = pageValidator(currentState, schema, setErrors, errors);
            setKey(++key)
            console.log(isValidated)
            if (isValidated) {
                await fakeAsyncCall(() => {
                    updateRequestState(true)
                }, 5000)
                updateRequestState(false);
                showStatus();
            }
        }}/>}
        {
            isMakingRequest && <div className={inputField.label}>Making Request...</div>
        }
        {
            showRequestStatus && <div className={inputField.label}>Submitted Successfully</div>
        }
    </div>
}