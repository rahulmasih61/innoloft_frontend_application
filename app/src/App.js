import React from 'react';
import body from './CSS Modules/body.module.css'
import EditingWindow from "./components/editing_window";
import SideNavigationTab from "./components/side_navigation_tab";
import NavBar from "./components/navbar";
import UserContextProvider from "./contexts/user";

function App() {
    return (
        <UserContextProvider>
            <div>
                <NavBar/>
                <div className={body.body}>
                    <div className={body.tab0}><SideNavigationTab/></div>
                    <div className={body.tab1}><EditingWindow/></div>
                </div>
            </div>
        </UserContextProvider>
    );
}

export default App;
