export async function fakeAsyncCall(callback,time) {
    callback();
    await new Promise(resolve => setTimeout(resolve, time));
}