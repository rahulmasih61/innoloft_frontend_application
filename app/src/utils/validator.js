import Joi from 'joi-browser'

export function pageValidator(currentState, schema, setter, currentErrorObject) {
    const {error} = Joi.validate(currentState, schema)
    if (!error) {
        return true;
    } else {
        let currentErrors = currentErrorObject;
        const details = error.details[0];
        const message = details.message;
        const key = details.context.key;
        currentErrors[key] = message;
        setter(currentErrors);
        return false
    }
}

export function fieldValidator(value,fieldName,errors,schema,setter) {

    let currentErrors = errors;
    const result = Joi.validate({[fieldName]: value}, {[fieldName]: schema[fieldName]})
    const {error} = result;
    if (!error) {
        currentErrors[fieldName] = null
        setter(currentErrors);
    } else {
        currentErrors[fieldName] = error.details[0].message
        setter(currentErrors)
    }
}