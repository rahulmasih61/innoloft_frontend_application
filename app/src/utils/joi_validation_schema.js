import Joi from "joi-browser";

export const PersonalDataSchema = {
    firstName: Joi.string().required().label('First Name'),
    lastName: Joi.string().required().label('Last Name'),
    street: Joi.string().required().label('Street'),
    houseNum: Joi.number().required().label('House Number'),
    postal: Joi.number().required().label('Postal Code'),
}