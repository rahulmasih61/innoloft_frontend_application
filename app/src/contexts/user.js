import React, {createContext, useState} from "react";

export const UserContext = createContext();

export default function UserContextProvider(props) {
    let [email, updateEmail] = useState('');
    let [country, setCountry] = useState('Germany');
    let [firstName, setFirstName] = useState('');
    let [lastName, setLastName] = useState('');
    let [street, setStreet] = useState('');
    let [houseNum, setHouseNum] = useState('');
    let [postal, setPostal] = useState('');

    let userData = {
        basicData: {
            values: {
                email: email
            },
            setters: {
                setEmail: updateEmail
            }
        },
        addressData: {
            values: {
                country: country,
                firstName: firstName,
                lastName: lastName,
                street: street,
                houseNum: houseNum,
                postal: postal
            },
            setters: {
                setCountry: setCountry,
                setFirstName: setFirstName,
                setLastName: setLastName,
                setStreet: setStreet,
                setHouseNum: setHouseNum,
                setPostal: setPostal
            }
        }
    };
    return (
        <UserContext.Provider className="Provider" value={{userData}}>
            {props.children}
        </UserContext.Provider>
    )
}